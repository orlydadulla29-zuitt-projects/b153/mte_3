let collection = [];

// Write the queue functions below.
// No ES6/Array methods allowed (anything with a .method() syntax)
// .length is allowed as it is a property, not a method

function print(){
	return collection
}

function enqueue(element){	
collection[collection.length] = element
console.log(collection)
}

function front(){
	console.log(collection[0])
}

function size(){
	console.log(collection.length)
}

function isEmpty(){
	if(collection.length === 0){
		return true
	}
}
module.exports = {
	collection,
	print,
	enqueue,
	front,
	size,
	isEmpty
};